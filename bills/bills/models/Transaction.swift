//
//  Transaction.swift
//  bills
//
//  Created by NFlahavan on 9/30/18.
//  Copyright © 2018 NFlahavan. All rights reserved.
//

import Foundation

struct transaction {
    var title: String
    var amount: Double
    var category: category
}

enum category {
    case entertainment
    case foodAndDrink
    case home
    case life
    case transportation
    case uncategorized
    case utilities
}
